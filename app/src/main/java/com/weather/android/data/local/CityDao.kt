package com.weather.android.data.local

import androidx.room.*
import com.weather.android.domain.model.CityItem

@Dao
interface CityDao {

    @Query("SELECT * FROM CityItem")
    suspend fun getCities(): List<CityItem>

    @Query("SELECT * FROM CityItem WHERE cityId=:id")
    suspend fun getCityById(id :Long) : CityItem?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCity(cityItem: CityItem) : Long?

    @Query("SELECT count(*) FROM CityItem")
    suspend fun getCartItemsCount(): Int?

    @Delete
    suspend fun delete(cityItem: CityItem)

    @Query("DELETE FROM CityItem")
    suspend fun deleteAll()
}
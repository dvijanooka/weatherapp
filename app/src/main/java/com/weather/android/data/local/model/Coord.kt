package com.weather.android.data.local.model

data class Coord(
    val lat: Double,
    val lon: Double
)
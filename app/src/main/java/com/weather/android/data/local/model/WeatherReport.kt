package com.weather.android.data.local.model

import android.annotation.SuppressLint
import android.os.Build
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.BindingAdapter
import com.weather.android.R
import com.weather.android.presentation.util.AppConstants.TIME_FORMAT
import com.weather.android.presentation.util.Utils
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.concurrent.TimeUnit

data class WeatherReport(
    val base: String,
    val clouds: Clouds,
    val cod: Int,
    val coord: Coord,
    val dt: Int,
    val id: Int,
    val main: Main,
    val name: String,
    val sys: Sys,
    val timezone: Int,
    val visibility: Int,
    val weather: List<Weather>,
    val wind: Wind
)

package com.shopping.android.feature.home.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.weather.android.data.local.CityDao
import com.weather.android.domain.model.CityItem

@Database(entities = [CityItem::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val cityDao: CityDao

    companion object {
        const val DATABASE_NAME = "weather_db"
    }
}
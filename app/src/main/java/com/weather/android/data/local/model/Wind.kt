package com.weather.android.data.local.model

data class Wind(
    val deg: Int,
    val speed: Double
)
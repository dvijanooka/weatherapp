package com.weather.android.data.local.model

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.weather.android.R

data class City(
    val coord: Coord,
    val country: String,
    val id: Long,
    val name: String,
    val state: String,
    var isWishListed: Boolean = false
)


@BindingAdapter("wishListImage")
fun loadWishListImage(view: ImageView, isWishListed: Boolean) {
    val id = if (isWishListed) R.drawable.ic_wishlist_blue else R.drawable.ic_wishlist_blue_outline
    view.setImageResource(id)
}

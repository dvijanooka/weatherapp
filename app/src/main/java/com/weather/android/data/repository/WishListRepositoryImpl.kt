package com.weather.android.data.repository

import com.weather.android.data.local.CityDao
import com.weather.android.domain.model.CityItem
import com.weather.android.domain.repository.WishListRepository

class WishListRepositoryImpl(
    private val cityDao: CityDao
) : WishListRepository {
    override suspend fun getCities(): List<CityItem> {
        return cityDao.getCities()
    }

    override suspend fun getCityById(id: Long): CityItem? {
        return cityDao.getCityById(id)
    }

    override suspend fun insertCity(cityItem: CityItem): Long? {

        return cityDao.insertCity(cityItem)
    }

    override suspend fun delete(cityItem: CityItem) {
        cityDao.delete(cityItem)
    }

    override suspend fun deleteAll() {
        cityDao.deleteAll()
    }
}
package com.weather.android.data.local.model

data class Clouds(
    val all: Int
)
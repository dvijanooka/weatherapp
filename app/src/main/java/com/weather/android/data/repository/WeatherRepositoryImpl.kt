package com.weather.android.data.repository

import com.weather.android.data.local.model.WeatherReport
import com.weather.android.data.remote.WeatherAPI
import com.weather.android.domain.repository.WeatherRepository

class WeatherRepositoryImpl(
    private val weatherAPI: WeatherAPI
) : WeatherRepository {
    override suspend fun getWeatherReport(id: Long, apiKey: String): WeatherReport? {
        return weatherAPI.getWeather(id, apiKey)
    }
}
package com.weather.android.data.remote

import com.weather.android.data.local.model.WeatherReport
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherAPI {

    @GET("weather")
    suspend fun getWeather(
        @Query("id") id: Long,
        @Query("apikey") apikey: String
    ): WeatherReport?
}
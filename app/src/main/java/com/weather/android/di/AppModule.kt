package com.weather.android.di

import android.app.Application
import androidx.room.Room
import com.shopping.android.feature.home.data.local.AppDatabase
import com.weather.android.data.remote.WeatherAPI
import com.weather.android.data.repository.WeatherRepositoryImpl
import com.weather.android.data.repository.WishListRepositoryImpl
import com.weather.android.domain.repository.WeatherRepository
import com.weather.android.domain.repository.WishListRepository
import com.weather.android.domain.usecase.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    const val BASE_URL = "https://api.openweathermap.org/data/2.5/"

    @Provides
    @Singleton
    fun provideAppDatabase(app: Application): AppDatabase {
        return Room.databaseBuilder(
            app,
            AppDatabase::class.java,
            AppDatabase.DATABASE_NAME
        ).build()
    }

    @Provides
    @Singleton
    fun provideWeatherApi(): WeatherAPI {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(WeatherAPI::class.java)
    }

    @Provides
    @Singleton
    fun provideCityRepository(db: AppDatabase): WishListRepository {
        return WishListRepositoryImpl(db.cityDao)
    }

    @Provides
    @Singleton
    fun provideWeatherRepository(api: WeatherAPI): WeatherRepository {
        return WeatherRepositoryImpl(api)
    }

    @Provides
    @Singleton
    fun provideWishListUseCases(wishListRepository: WishListRepository): WishListedUseCases {
        return WishListedUseCases(
            addCityToWishListUseCase = AddCityToWishListUseCase(wishListRepository),
            getWishListedCitiesUseCase = GetWishListedCitiesUseCase(wishListRepository),
            getWishListedCityByIdUseCase = GetWishListedCityByIdUseCase(wishListRepository)
        )
    }

    @Provides
    @Singleton
    fun provideWeatherUseCases(weatherRepository: WeatherRepository): GetWeatherUseCase {
        return GetWeatherUseCase(weatherRepository)
    }

}
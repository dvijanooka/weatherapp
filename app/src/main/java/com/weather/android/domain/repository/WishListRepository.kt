package com.weather.android.domain.repository

import com.weather.android.domain.model.CityItem

interface WishListRepository {
    suspend fun getCities(): List<CityItem>

    suspend fun getCityById(id: Long): CityItem?

    suspend fun insertCity(cityItem: CityItem): Long?

    suspend fun delete(cityItem: CityItem)

    suspend fun deleteAll()
}
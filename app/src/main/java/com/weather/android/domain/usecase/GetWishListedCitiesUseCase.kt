package com.weather.android.domain.usecase

import com.weather.android.domain.model.CityItem
import com.weather.android.domain.repository.WishListRepository

class GetWishListedCitiesUseCase(
    private val wishListRepository: WishListRepository
) {
    suspend operator fun invoke(
    ): List<CityItem> {
        return wishListRepository.getCities()
    }
}
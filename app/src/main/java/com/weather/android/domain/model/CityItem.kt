package com.weather.android.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class CityItem(
    @PrimaryKey
    val cityId: Long = -1,
    val cityName: String? = null,
    val state: String? = null,
    val country: String? = null,
    val lat: Double,
    val lon: Double
)
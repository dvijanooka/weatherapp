package com.weather.android.domain.usecase

import com.weather.android.data.local.model.City
import com.weather.android.domain.model.CityItem
import com.weather.android.domain.repository.WishListRepository

class AddCityToWishListUseCase(
    private val wishListRepository: WishListRepository
) {
    suspend operator fun invoke(city:City): Long? {
        val cityByID = wishListRepository.getCityById(city.id)
        val cityItem =
            CityItem(city.id, city.name, city.state, city.country, city.coord.lat, city.coord.lon)

        return if (cityByID != null) {
            wishListRepository.delete(cityItem)
            0
        } else {
            wishListRepository.insertCity(cityItem)
        }
    }
}
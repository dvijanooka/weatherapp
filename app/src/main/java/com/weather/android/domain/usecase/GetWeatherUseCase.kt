package com.weather.android.domain.usecase

import com.weather.android.data.local.model.WeatherReport
import com.weather.android.domain.repository.WeatherRepository

class GetWeatherUseCase(
    private val weatherRepository: WeatherRepository
) {
    suspend operator fun invoke(id: Long, apiKey: String): WeatherReport? {
        return weatherRepository.getWeatherReport(id, apiKey)
    }
}
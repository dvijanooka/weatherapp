package com.weather.android.domain.usecase

data class WishListedUseCases(
    val addCityToWishListUseCase: AddCityToWishListUseCase,
    val getWishListedCitiesUseCase: GetWishListedCitiesUseCase,
    val getWishListedCityByIdUseCase: GetWishListedCityByIdUseCase
)

package com.weather.android.domain.usecase

import com.weather.android.domain.model.CityItem
import com.weather.android.domain.repository.WishListRepository

class GetWishListedCityByIdUseCase(
    private val wishListRepository: WishListRepository
) {

    suspend operator fun invoke(id: Long): CityItem? {
        return wishListRepository.getCityById(id)
    }
}
package com.weather.android.domain.repository

import com.weather.android.data.local.model.WeatherReport

interface WeatherRepository {
    suspend fun getWeatherReport(id: Long, apiKey: String) : WeatherReport?
}
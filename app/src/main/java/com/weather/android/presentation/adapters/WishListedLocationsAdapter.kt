package com.weather.android.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.weather.android.databinding.ItemWishlistedCityLayoutBinding
import com.weather.android.domain.model.CityItem

class WishListedLocationsAdapter(
    private val citiesList: ArrayList<CityItem>
) : RecyclerView.Adapter<WishListedLocationsAdapter.DataViewHolder>() {

    interface OnClickListener {
        fun onLocationClicked(id: Long)
    }

    private var mClickListener: OnClickListener? = null

    class DataViewHolder(val binding: ItemWishlistedCityLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setClickListener(callback: OnClickListener) {
        mClickListener = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            ItemWishlistedCityLayoutBinding.inflate(LayoutInflater.from(parent.context))
        )

    override fun getItemCount(): Int = citiesList.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {

        val city = citiesList[position]

        holder.binding.model = city
        holder.binding.executePendingBindings()

        holder.binding.constraintMain.setOnClickListener {
            mClickListener?.onLocationClicked(city.cityId)
        }
    }

    fun addData(list: List<CityItem>) {
        citiesList.clear()
        citiesList.addAll(list)
    }

}


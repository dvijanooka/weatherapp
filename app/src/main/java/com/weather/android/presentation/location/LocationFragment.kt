package com.weather.android.presentation.location

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.AssetManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.weather.android.data.local.model.City
import com.weather.android.databinding.FragmentLocationBinding
import com.weather.android.presentation.adapters.LocationsAdapter
import dagger.hilt.android.AndroidEntryPoint
import java.io.IOException
import java.io.InputStream
import java.lang.reflect.Type

@AndroidEntryPoint
class LocationFragment : Fragment(), LocationsAdapter.OnClickListener {

    private lateinit var binding: FragmentLocationBinding
    private val viewModel: LocationViewModel by viewModels()

    private val adapter = LocationsAdapter(arrayListOf())
    private var cityList = emptyList<City>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentLocationBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAdapter()
        loadAddFromAssets()

        binding.locationNameEdiText.doOnTextChanged { text, _, _, _ ->
            if (cityList.isNotEmpty()) {
                val list =
                    cityList.filter { it.name.lowercase().contains(text.toString().lowercase()) }
                renderList(list)
            }
        }

        viewModel.wishListedLocationId.observeForever { id ->
            activity?.onBackPressed()
        }
    }

    private fun loadAddFromAssets() {
        val jsonString = context?.let { getAssetJSONFile("city.list.json", it) }
        val listType: Type = object : TypeToken<List<City?>?>() {}.type
        val list: List<City>? = Gson().fromJson(jsonString, listType)
        if (list != null && list.isNotEmpty()) {
            cityList = list.filter { it.country == "NL" }.sortedBy { it.name }
        }
    }

    @Throws(IOException::class)
    fun getAssetJSONFile(filename: String?, context: Context): String? {
        val manager: AssetManager = context.assets
        val file: InputStream? = filename?.let { manager.open(it) }
        val formArray = file?.available()?.let { ByteArray(it) }
        file?.read(formArray)
        file?.close()
        return formArray?.let { String(it) }
    }


    private fun setAdapter() {
        binding.adapter = adapter

        adapter.setClickListener(this)
    }

    override fun onWishListClicked(city: City) {
        viewModel.insertCity(city)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(cities: List<City>) {
        adapter.addData(cities)
        adapter.notifyDataSetChanged()
    }

}
package com.weather.android.presentation.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weather.android.domain.model.CityItem
import com.weather.android.domain.usecase.WishListedUseCases
import com.weather.android.presentation.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val wishListedUseCases: WishListedUseCases
): ViewModel() {

    private val wishListedLocationsLiveData = MutableLiveData<Resource<List<CityItem>>>()
    val wishListedLocations: LiveData<Resource<List<CityItem>>> = wishListedLocationsLiveData

    fun getWishListedCities(){
        viewModelScope.launch {
           val cities =  wishListedUseCases.getWishListedCitiesUseCase.invoke()
            wishListedLocationsLiveData.postValue(Resource.Success(cities))
        }
    }
}
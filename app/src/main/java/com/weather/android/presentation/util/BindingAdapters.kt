package com.weather.android.presentation.util

import android.annotation.SuppressLint
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.weather.android.R
import com.weather.android.data.local.model.WeatherReport
import java.text.SimpleDateFormat

@BindingAdapter("tempText")
fun setTemperatureText(textView: TextView, tempInKelvin: Double) {
    val temperatureInCelsius = Utils.kelvinToCelsius(tempInKelvin)
    textView.text =
        textView.context.getString(R.string.temp_celsius, temperatureInCelsius.toInt())
}

@BindingAdapter("tempTextDegree")
fun setTemperatureDegreeText(textView: TextView, tempInKelvin: Double) {
    val temperatureInCelsius = Utils.kelvinToCelsius(tempInKelvin)
    textView.text =
        textView.context.getString(R.string.temp_degree, temperatureInCelsius.toInt())
}

@BindingAdapter("realFeelTextDegree")
fun setRealFeelTempDegreeText(textView: TextView, tempInKelvin: Double) {
    val temperatureInCelsius = Utils.kelvinToCelsius(tempInKelvin)
    textView.text =
        textView.context.getString(R.string.real_feel_temp_degree, temperatureInCelsius.toInt())
}

@BindingAdapter("descriptionText")
fun setDescriptionText(textView: TextView, model: WeatherReport?) {
    if (model?.weather != null && model.weather.isNotEmpty()) {
        textView.text = model.weather.first().description
    }
}

@SuppressLint("SimpleDateFormat")
@BindingAdapter("timeText")
fun setTimeText(textView: TextView, millis: Int?) {
    if (millis != null) {

        val simpleDateFormat = SimpleDateFormat(AppConstants.TIME_FORMAT)
        val dateString = simpleDateFormat.format(millis)

        textView.text = dateString
    }
}

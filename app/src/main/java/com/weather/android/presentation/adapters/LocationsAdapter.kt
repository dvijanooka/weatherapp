package com.weather.android.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.weather.android.data.local.model.City
import com.weather.android.data.local.model.loadWishListImage
import com.weather.android.databinding.ItemCityLayoutBinding

class LocationsAdapter(
    private val citiesList: ArrayList<City>
) : RecyclerView.Adapter<LocationsAdapter.DataViewHolder>() {

    interface OnClickListener {
        fun onWishListClicked(city: City)
    }

    private var mClickListener: OnClickListener? = null

    class DataViewHolder(val binding: ItemCityLayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setClickListener(callback: OnClickListener) {
        mClickListener = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DataViewHolder(
            ItemCityLayoutBinding.inflate(LayoutInflater.from(parent.context))
        )

    override fun getItemCount(): Int = citiesList.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {

        val city = citiesList[position]

        holder.binding.model = city
        holder.binding.executePendingBindings()

        holder.binding.wishListImage.setOnClickListener {
            city.isWishListed = !city.isWishListed
            loadWishListImage(holder.binding.wishListImage, city.isWishListed)
            mClickListener?.onWishListClicked(city)
        }
    }

    fun addData(list: List<City>) {
        citiesList.clear()
        citiesList.addAll(list)
    }

}


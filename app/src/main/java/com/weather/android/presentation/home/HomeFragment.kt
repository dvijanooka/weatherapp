package com.weather.android.presentation.home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.weather.android.R
import com.weather.android.databinding.FragmentHomeBinding
import com.weather.android.domain.model.CityItem
import com.weather.android.presentation.adapters.WishListedLocationsAdapter
import com.weather.android.presentation.util.AppConstants.COUNTRY_ID
import com.weather.android.presentation.util.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment(), WishListedLocationsAdapter.OnClickListener {

    private lateinit var binding: FragmentHomeBinding
    private val viewModel: HomeViewModel by viewModels()
    private val adapter = WishListedLocationsAdapter(arrayListOf())

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.addLocationButton.setOnClickListener {
            findNavController().navigate(R.id.action_HomeFragment_to_LocationFragment)
        }

        setAdapter()
        setUpObservers()
    }

    private fun setUpObservers() {
        viewModel.wishListedLocations.observeForever { result ->
            when (result) {
                is Resource.Success -> {
                    result.data?.let { renderList(it) }
                }
                is Resource.Error -> {

                }
            }
        }
    }

    private fun setAdapter() {
        binding.adapter = adapter

        adapter.setClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        viewModel.getWishListedCities()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun renderList(cities: List<CityItem>) {
        adapter.addData(cities)
        adapter.notifyDataSetChanged()
    }

    override fun onLocationClicked(id: Long) {
        val args = Bundle().apply {
            putLong(COUNTRY_ID, id)
        }
        findNavController().navigate(R.id.action_HomeFragment_to_WeatherFragment, args)
    }
}
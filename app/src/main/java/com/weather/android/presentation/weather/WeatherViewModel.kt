package com.weather.android.presentation.weather

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weather.android.data.local.model.WeatherReport
import com.weather.android.domain.usecase.GetWeatherUseCase
import com.weather.android.presentation.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WeatherViewModel @Inject constructor(
    private val weatherUseCase: GetWeatherUseCase
) : ViewModel() {

    private val weatherReportLiveData = MutableLiveData<Resource<WeatherReport>>()
    val weatherReport: LiveData<Resource<WeatherReport>> = weatherReportLiveData

    private val loadingLiveData = MutableLiveData<Int>()
    val loading: LiveData<Int> = loadingLiveData

    fun getWeatherReport(id: Long, apikey: String) {
        loadingLiveData.postValue(View.VISIBLE)
        viewModelScope.launch {
            val report = weatherUseCase.invoke(id, apikey)
            if (report != null) {
                loadingLiveData.postValue(View.GONE)
                weatherReportLiveData.postValue(Resource.Success(report))
            } else {
                loadingLiveData.postValue(View.GONE)
            }
        }
    }
}
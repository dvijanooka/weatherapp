package com.weather.android.presentation.location

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.weather.android.data.local.model.City
import com.weather.android.domain.usecase.WishListedUseCases
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LocationViewModel @Inject constructor(
    private val wishListedUseCases: WishListedUseCases
) : ViewModel() {

    private val wishListedLocationIdLiveData = MutableLiveData<Long?>()
    val wishListedLocationId: LiveData<Long?> = wishListedLocationIdLiveData

    fun insertCity(city: City) {
        viewModelScope.launch {
            val id = wishListedUseCases.addCityToWishListUseCase.invoke(city)
            wishListedLocationIdLiveData.postValue(id)
        }
    }
}
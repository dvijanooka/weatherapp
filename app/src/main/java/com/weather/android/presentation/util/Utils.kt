package com.weather.android.presentation.util

object Utils {

    fun kelvinToCelsius(temp: Double): Double {
        return temp-273.15
    }
}
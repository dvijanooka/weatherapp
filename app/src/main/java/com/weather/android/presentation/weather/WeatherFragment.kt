package com.weather.android.presentation.weather

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.weather.android.databinding.FragmentWeatherBinding
import com.weather.android.presentation.util.AppConstants.API_KEY
import com.weather.android.presentation.util.AppConstants.COUNTRY_ID
import com.weather.android.presentation.util.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class WeatherFragment : Fragment() {

    private lateinit var binding: FragmentWeatherBinding
    private val viewModel: WeatherViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel

        val id = arguments?.getLong(COUNTRY_ID)

        if (id != null) {
            viewModel.getWeatherReport(id, API_KEY)
        }

        viewModel.weatherReport.observeForever { result ->
            when (result) {
                is Resource.Success -> {
                    binding.constraintMain.visibility = View.VISIBLE
                    binding.errorConstraint.visibility = View.GONE
                    val report = result.data
                    if (report != null) {
                        binding.model = report
                    }
                }
                is Resource.Error -> {
                    binding.constraintMain.visibility = View.GONE
                    binding.errorConstraint.visibility = View.VISIBLE
                }
            }
        }
    }

}